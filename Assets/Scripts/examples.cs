using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Data;


//In the Scene, select your canvas object  and add component-> Script > dbAccess.cs and this one (main or your own)  

public class examples : MonoBehaviour {

    //object to receive data from query
    DataTable table;
    //prepare the object to connect to sqlitedb
    private Sqlite db;
    //Put here the UI Widgets or GameObjects you want to update with SQL data. 
    private Text text1, text2, textLog;
	private InputField InputField1, InputField2;
	private Button buttonInsert, buttonEdit, buttonSearch, buttonDelete;
	


    DataTable Query(string sql)
    {
        string dbPath = System.IO.Path.Combine(Application.persistentDataPath, "db.sqlite");
        //create the connection to db
        Sqlite db = new Sqlite(dbPath);
        return db.Query(sql);
    }

    void AlterQuery(string sql)
    {
        string dbPath = System.IO.Path.Combine(Application.persistentDataPath, "db.sqlite");
        //create the connection to db
        Sqlite db = new Sqlite(dbPath);
        db.AlterQuery(sql);
    }

    // Use this for initialization
    void Start () {
		Debug.Log("starting  app");
        //binding to objects in scene what you interact in script
        text1 = GameObject.Find("text1").GetComponent<Text>();
        text2 = GameObject.Find("text2").GetComponent<Text>();
        textLog = GameObject.Find("textLog").GetComponent<Text>();

        InputField1 = GameObject.Find("InputField1").GetComponent<InputField>();
        InputField2 = GameObject.Find("InputField2").GetComponent<InputField>();

        buttonInsert = GameObject.Find("ButtonInsert").GetComponent<Button>();
        buttonSearch = GameObject.Find("ButtonSearch").GetComponent<Button>();
        buttonEdit = GameObject.Find("ButtonEdit").GetComponent<Button>();
        buttonDelete = GameObject.Find("ButtonDelete").GetComponent<Button>();


        //simple sample query 
        table = Query("select field1,field2 from table1 where field1='1'");

        foreach (DataRow row in table.Rows)
        {
            //update the text fields with row values
            text1.text = row["field1"].ToString();
            text2.text = row["field2"].ToString();

        }



		//actions to do when click on button
		buttonSearch.onClick.AddListener(() => { Search(InputField1.text.ToString(),InputField2.text.ToString());});
		buttonInsert.onClick.AddListener(() => { Insert(InputField1.text.ToString(),InputField2.text.ToString());});
		buttonEdit.onClick.AddListener(() => { Edit(InputField1.text.ToString(),InputField2.text.ToString());});
		buttonDelete.onClick.AddListener(() => { Delete(InputField1.text.ToString(),InputField2.text.ToString());});
	
				
	}

	public void Insert(string field1,string field2){


		string q;
		if (field1!="" && field2 !=""){
			q = "insert into table1  (field1,field2) values ('"+field1+"','"+field2+"');";

			Debug.Log(q);

			AlterQuery(q); 

        	textLog.text = "Inserted 1 row";
        }
     
	}


	public void Edit(string field1,string field2){


		string q;
		if (field1!="" && field2 !=""){
			q = "update  table1  set field1='"+field1+"',field2='"+field2+"' where field1='"+text1.text.ToString()+"' and field2='"+text2.text.ToString()+"'";

			Debug.Log(q);

			AlterQuery(q); 

        	textLog.text = "updated 1 row";
        }
        
	}

	public void Search(string field1,string field2){


		string q;
		if (field1!="" && field2 !=""){
			q = "select field1,field2 from table1 where field1='"+field1+"' and field2='"+field2+"'";
		} else if (field1!="") {
			q = "select field1,field2 from table1 where field1="+field1+";";
		} else if (field2!="") {
			q = "select field1,field2 from table1 where field2='"+field2+"'";
		} else {
			q = "select * from table1";
		}
			DataTable table = Query(q);

        int i = 0;
        foreach (DataRow row in table.Rows)
        {
            //rows now is a row. rows[0] retrieves first field
            InputField1.text = row["field1"].ToString();
            InputField2.text = row["field2"].ToString();
            text1.text = row["field1"].ToString();
            text2.text = row["field2"].ToString();

            Debug.Log("Text Values updated");
            i = i + 1;
        }
        textLog.text = "selected "+i+" row(s). Only the last one is showed to edit or delete. "+q;
	}	


	public void Delete(string field1,string field2){

		string q = "";
		if (field1!="" && field2 !=""){
			q = "delete from table1 where field1='"+field1+"' and field2='"+field2+"' limit='1' ";
		} else if (field1!="") {
			q = "delete from table1 where field1='"+field1+ "' limit='1' ";
        } else if (field2!="") {
			q = "delete from table1 where field2='"+field2+ "' limit='1' ";
		} 
		if (q!=""){
			AlterQuery(q);
		}

       		textLog.text = "deleted where field1='"+field1+"' and/or field2='"+field2+"'";

	}
	// Update is called once per frame
	void Update () {
		
			
	}
	
}	
